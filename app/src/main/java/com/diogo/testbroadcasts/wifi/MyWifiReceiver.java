package com.diogo.testbroadcasts.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import static android.content.ContentValues.TAG;
import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Testing a long task in a broadcastReceiver.
 *
 * Some devices trigger this event multiple times.
 */
public class MyWifiReceiver extends BroadcastReceiver {

    private boolean isOK;

    @Override
    public void onReceive(final Context context, Intent intent) {
        isOK = isConnectedToInternet(context);
        Log.i("app", "onReceive: isOK: " + isOK);

        if (isOK) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isOK = isConnectedToInternet(context);
                    if (isOK) {
                        Log.i("app", "run: still valid");

                        final PendingResult pendingResult = goAsync();
                        AsyncTask<String, Integer, String> asyncTask = new AsyncTask<String, Integer, String>() {
                            @Override
                            protected String doInBackground(String... params) {
                                //EXECUTE A LONG TASK HERE
                                try {
                                    Thread.sleep(10000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                pendingResult.finish();
                                return "DONE";
                            }
                        };
                        asyncTask.execute();

                    }
                }
            }, 5000);
        }


    }

    //UTILS

    private boolean isConnectedToInternet(Context context) {
        try {
            if (context != null) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }
            return false;
        } catch (Exception e) {
            Log.e("app", e.getMessage());
            return false;
        }
    }


}
