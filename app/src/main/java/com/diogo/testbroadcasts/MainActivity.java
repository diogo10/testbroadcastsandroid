package com.diogo.testbroadcasts;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.diogo.testbroadcasts.airplanemode.AirPlaneModeActivity;
import com.diogo.testbroadcasts.localbroadcaster.LocalBroadcasterActivity;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_name));

        ArrayAdapter<String> items = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                new String[]{"Airplane mode (Context broadcaster)",
                        "Wifi broadcaster (No UI)",
                        "Local broadcaster"});

        setListAdapter(items);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 1) return;

        Intent it = null;
        switch (position) {
            case 0:
                it = new Intent(getApplicationContext(), AirPlaneModeActivity.class);
                break;
            case 2:
                it = new Intent(getApplicationContext(), LocalBroadcasterActivity.class);
                break;
        }

        startActivity(it);
    }


}
