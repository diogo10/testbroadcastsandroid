package com.diogo.testbroadcasts.airplanemode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

/**
 * Custom Airplane Broadcast (Context registered receivers).
 * It should be implemented on the activity in order to it knows the state(on or off).
 *
 * https://developer.android.com/guide/components/broadcasts.html
 *
 * @author Diogo
 */
public abstract class MyAirPlaneModeReceiver extends BroadcastReceiver {

    private  Context context;

    public MyAirPlaneModeReceiver(Context ctx){
        this.context = ctx;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        
        if (intent.getBooleanExtra("state",false)){
            Log.i("app", "onReceive: it's on");
            airPlaneModeChanged(true);
        }else{
            Log.i("app", "onReceive: it's off");
            airPlaneModeChanged(false);
        }
    }

    //UTILS

    public void register(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        context.registerReceiver(this,intentFilter);
    }

    public void unRegister(){
        context.unregisterReceiver(this);
    }

    //LISTENER

    public abstract void airPlaneModeChanged(boolean mode);
}
