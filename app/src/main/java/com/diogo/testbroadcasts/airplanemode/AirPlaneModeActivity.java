package com.diogo.testbroadcasts.airplanemode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import com.diogo.testbroadcasts.R;
import java.util.Locale;

public class AirPlaneModeActivity extends AppCompatActivity {

    private TextView textView;
    private MyAirPlaneModeReceiver myAirPlaneModeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_plane_mode);
        textView = (TextView) findViewById(R.id.textView);

        myAirPlaneModeReceiver= new MyAirPlaneModeReceiver(this) {
            @Override
            public void airPlaneModeChanged(boolean mode) {
                String isOn = ((mode) ? "YES" : "NO");
                textView.setText(String.format(Locale.ENGLISH,"Air Plane Mode is on ? %s", isOn));
            }
        };

        myAirPlaneModeReceiver.register();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myAirPlaneModeReceiver.unRegister();
    }
}
